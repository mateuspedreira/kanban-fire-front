import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { transferArrayItem } from '@angular/cdk/drag-drop';
import { TaskDialogComponent } from './task-dialog/task-dialog.component';
import { BehaviorSubject } from 'rxjs';
const getObservable = (collection) => {
    const subject = new BehaviorSubject([]);
    collection.valueChanges({ idField: 'id' }).subscribe((val) => {
        subject.next(val);
    });
    return subject;
};
let AppComponent = class AppComponent {
    constructor(dialog, store) {
        this.dialog = dialog;
        this.store = store;
        this.todo = this.store.collection('todo').valueChanges({ idField: 'id' });
        this.inProgress = this.store.collection('inProgress').valueChanges({ idField: 'id' });
        this.done = this.store.collection('done').valueChanges({ idField: 'id' });
    }
    title(title) {
        throw new Error('Method not implemented.');
    }
    newTask() {
        const dialogRef = this.dialog.open(TaskDialogComponent, {
            width: '270px',
            data: {
                task: {},
            },
        });
        dialogRef
            .afterClosed()
            .subscribe((result) => {
            if (!result) {
                return;
            }
            this.store.collection('todo').add(result.task);
        });
    }
    editTask(list, task) {
        const dialogRef = this.dialog.open(TaskDialogComponent, {
            width: '270px',
            data: {
                task,
                enableDelete: true,
            },
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (!result) {
                return;
            }
            if (result.delete) {
                this.store.collection(list).doc(task.id).delete();
            }
            else {
                this.store.collection(list).doc(task.id).update(task);
            }
        });
    }
    drop(event) {
        if (event.previousContainer === event.container) {
            return;
        }
        if (!event.previousContainer.data || !event.container.data) {
            return;
        }
        const item = event.previousContainer.data[event.previousIndex];
        this.store.firestore.runTransaction(() => {
            const promise = Promise.all([
                this.store.collection(event.previousContainer.id).doc(item.id).delete(),
                this.store.collection(event.container.id).add(item),
            ]);
            return promise;
        });
        transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }
};
AppComponent = __decorate([
    Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css']
    })
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map