// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// import { initializeApp } from "firebase/app";

export const environment = {
  production: false,
  firebase: {
    projectId: 'kanbanfire-52af3',
    appId: '1:614961131550:web:9bf457a076eaed92e1fe0f',
    storageBucket: 'kanbanfire-52af3.appspot.com',
    locationId: 'southamerica-east1',
    apiKey: 'AIzaSyBsQ21sG2XJcVhLZ64WticWAh6vnD2Edus',
    authDomain: 'kanbanfire-52af3.firebaseapp.com',
    messagingSenderId: '614961131550',
  }
};

// const firebaseConfig = {
//   apiKey: "AIzaSyBsQ21sG2XJcVhLZ64WticWAh6vnD2Edus",
//   authDomain: "kanbanfire-52af3.firebaseapp.com",
//   projectId: "kanbanfire-52af3",
//   storageBucket: "kanbanfire-52af3.appspot.com",
//   messagingSenderId: "614961131550",
//   appId: "1:614961131550:web:9bf457a076eaed92e1fe0f"
// };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
