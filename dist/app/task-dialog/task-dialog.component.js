import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
let TaskDialogComponent = class TaskDialogComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.backupTask = Object.assign({}, this.data.task);
    }
    cancel() {
        this.data.task.title = this.backupTask.title;
        this.data.task.description = this.backupTask.description;
        this.dialogRef.close(this.data);
    }
    ngOnInit() {
    }
};
TaskDialogComponent = __decorate([
    Component({
        selector: 'app-task-dialog',
        templateUrl: './task-dialog.component.html',
        styleUrls: ['./task-dialog.component.css'],
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], TaskDialogComponent);
export { TaskDialogComponent };
//# sourceMappingURL=task-dialog.component.js.map