import { __decorate } from "tslib";
// import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Component, Input, Output, EventEmitter } from '@angular/core';
let TaskComponent = 
// export class TaskComponent {
class TaskComponent {
    constructor() {
        this.task = null;
        this.edit = new EventEmitter();
    }
    ngOnInit() {
    }
};
__decorate([
    Input()
], TaskComponent.prototype, "task", void 0);
__decorate([
    Output()
], TaskComponent.prototype, "edit", void 0);
TaskComponent = __decorate([
    Component({
        selector: 'app-task',
        templateUrl: './task.component.html',
        styleUrls: ['./task.component.css']
    })
    // export class TaskComponent {
], TaskComponent);
export { TaskComponent };
//# sourceMappingURL=task.component.js.map